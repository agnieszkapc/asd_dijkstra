package edu.pjwstk.asd.dijkstra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class DijkstraAlg {

	/**
	 * Odnajduje nakrótszą ścieżkę od wierzchołka start do wierzchołka end w grafie graph.
	 * @param graph
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<Vertice> findShortestPath (Graph graph, Vertice start, Vertice end) {
		System.out.println("Szukam ścieżki od wierzchołka "+start+" do wierzchołka "+end+".");
		
		Set<Vertice> Q = new HashSet<Vertice>();
		start.distance = 0;
		Q.add(start);
		while(!Q.isEmpty()){
			Vertice current = getMin(Q);
			if(end == current)
				return buildPath(current, start);
			double currentDist = current.distance;
			for(Entry<Vertice,Double> ngbr: current.getNeighbours().entrySet()) {
				double newDist = ngbr.getValue() + currentDist;
				if (newDist<ngbr.getKey().distance) {
					ngbr.getKey().distance = newDist;
					ngbr.getKey().previous = current;
				}
				if (!ngbr.getKey().analyzed==true)
					Q.add(ngbr.getKey());
			}
			current.analyzed = true;
			Q.remove(current);
		}
		
		return null;
	}

	private static List<Vertice> buildPath(Vertice end, Vertice start) {
		List<Vertice> ret = new ArrayList<Vertice>();
		Vertice current = end;
		while(true) {
			ret.add(current);
			if(current == start) break;
			if(current.previous == null)
				throw new Error("Coś nie tak");
			current = current.previous;
		}
		Collections.reverse(ret);
		return ret;
	}

	private static Vertice getMin(Set<Vertice> set) {
		double minDist = Double.MAX_VALUE;
		Vertice ret = null;
		for(Vertice v: set)
			if(v.distance < minDist) {
				ret = v;
				minDist = v.distance;
			}
		return ret;
	}
	
	
	
}
