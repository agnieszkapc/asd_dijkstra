package edu.pjwstk.asd.dijkstra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Graph {

	Set<Vertice> vertices;

	public Graph(Set<Vertice> vertices) {
		this.vertices = vertices;
	}
	
	public Graph(Vertice[] vertices) {
		this.vertices = new HashSet<Vertice>(Arrays.asList(vertices));
	}

	/**
	 * Metoda tworząca przykładowy graf.
	 * Jest to graf identyczny z przykładowym grafem z wikipedii:
	 * http://pl.wikipedia.org/wiki/Algorytm_Dijkstry
	 */
	public static Graph createExemplaryGraph() {
		Vertice v1 = new Vertice("1");
		Vertice v2 = new Vertice("2");
		Vertice v3 = new Vertice("3");
		Vertice v4 = new Vertice("4");
		Vertice v5 = new Vertice("5");
		Vertice v6 = new Vertice("6");
		
		v1.addNeighbour(v2, 7);
		v1.addNeighbour(v3, 9);
		v1.addNeighbour(v6, 14);
		v2.addNeighbour(v4, 15);
		v3.addNeighbour(v4, 11);
		v3.addNeighbour(v6, 2);
		v4.addNeighbour(v5, 6);
		v5.addNeighbour(v6, 9);
		
		return new Graph(new Vertice[]{v1,v2,v3,v4,v5,v6});
	}
	
	/**
	 * Metoda zwraca graf z tablicy z zajęć.
	 * @return
	 */
	public static Graph createMoreInterestingGraph() {
		Vertice[] ret = new Vertice[11];
		ret[0] = new Vertice("0");
		for(int i=1; i<= 10; i++){
			ret[i] = new Vertice(""+i);
		}
		ret[1].addNeighbour(ret[5], 2);
		ret[1].addNeighbour(ret[3], 2);
		ret[1].addNeighbour(ret[2], 4);
		ret[2].addNeighbour(ret[4], 2);
		ret[3].addNeighbour(ret[7], 2);
		ret[3].addNeighbour(ret[6], 1.5);
		ret[4].addNeighbour(ret[7], 1.5);
		ret[5].addNeighbour(ret[10], 1);
		ret[6].addNeighbour(ret[10], 1);
		ret[6].addNeighbour(ret[9], 7);
		ret[6].addNeighbour(ret[8], 3);
		ret[7].addNeighbour(ret[8], 2.5);
		ret[8].addNeighbour(ret[9], 2);
		return new Graph(ret);
	}

	public Vertice getVerticeById(String id) {
		for(Vertice v: vertices)
			if(v.getId().equals(id))
				return v;
		return null;
	}
	
}
