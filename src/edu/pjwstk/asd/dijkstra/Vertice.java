package edu.pjwstk.asd.dijkstra;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Vertice {

	/**
	 * Id (nazwa) wierzchołka.
	 */
	private String id;
	
	/**
	 * Mapa sąsiednich wierzchołków.
	 * Kluczem jest wierzchołek a wartością jest odległość do niego.
	 */
	private Map<Vertice,Double> neighbours;
	
	/**
	 * Pola związane ściśle z algorytmem Dijkstry:
	 * poprzednik (na najkrótszej znalezionej do tej pory ścieżce
	 * odległość od początkowego wierzchołka
	 */
	Vertice previous;
	double distance = Double.MAX_VALUE;
	boolean analyzed = false;
	
	/**
	 * Konstruktor
	 * @param id nazwa wierzchołka
	 */
	public Vertice(String id) {
		this.id = id;
		neighbours = new HashMap<Vertice, Double>();
	}

	/**
	 * Metoda dodaje sąsiada danego wierzchołka.
	 * Dba o to, żeby relacja była obustronna =>
	 * nie trzeba pisać v1.addNeighbour(v2,...) i v2.addNeighbour(v1,...) - wystarczy jedna (dowolna) z tych instrukcji.
	 * @param v sąsiad do dodania
	 * @param distance odległość między wierzchołkami
	 */
	public void addNeighbour(Vertice v, double distance) {
		neighbours.put(v, distance);
		v.neighbours.put(this, distance);
	}

	public Map<Vertice,Double> getNeighbours() {
		return neighbours;
	}
	
	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder("\"" + id + "\" (sąsiedzi: ");
		String c = "";
		for(Entry<Vertice, Double> n : neighbours.entrySet()) {
			ret.append(c);
			ret.append("\""+n.getKey().getId()+"\" "+n.getValue());
			c = " ,";
		}
		return  ret.append(")").toString();
	}
	
}
