package edu.pjwstk.asd.dijkstra;

import java.util.List;

public class Main {

	public static void main(String[] args) {
	
		//Przypadek identyczny z tym ze strony http://pl.wikipedia.org/wiki/Algorytm_Dijkstry
//		Graph graph = Graph.createExemplaryGraph();
//		List<Vertice> path1 = DijkstraAlg.findShortestPath(
//				graph, graph.getVerticeById("1"), graph.getVerticeById("5"));
//		printPath(path1);
		
		//Przypadek z tablicy z lab.
		Graph graph2 = Graph.createMoreInterestingGraph();
		List<Vertice> path2 = DijkstraAlg.findShortestPath(
				graph2, graph2.getVerticeById("1"), graph2.getVerticeById("9"));
		printPath(path2);
		
	}

	private static void printPath(List<Vertice> path) {
		for(Vertice v: path) {
			System.out.println("\""+v.getId() +"\"");
		}
		System.out.println(path.get(path.size()-1).distance);
	}
	
}
